const serverLocation = "http://localhost:9173";

$(document).ready(function () {



    getFoods();

});



function getFoods() {

    $.ajax({
        type: 'post',
        url: serverLocation + "/api/mobile/getRestaurantAllMealData",
        dataType: 'json',
        data: JSON.stringify({ "restaurantId": localStorage.getItem('restaurant') }),
        contentType: "application/json; charset=UTF-8",
        success: function (response) {
            fillFoods(response.MealData);
        },
        error: function (textStatus, errorThrown) {
            console.log('Err');
        }
    });

}




function fillFoods(data) {


    $.each(data, function (i, obj) {
        var mealName = obj.MealName;
        var minAmount = obj.MealMinAmount;
        var maxAmount = obj.MealMaxAmount;

        var priceString = "";
        if (minAmount == maxAmount) {
            priceString = "Rs. " + minAmount;
        } else {
            priceString = "Rs. " + minAmount + " - " + maxAmount;
        }


        var htmlData = '<a href="http://iglyphic.com/themes/html/html_thefoody/?#" class="restaurant-common-wrapper-item highlight clearfix"> ' +
            '<div class="GridLex-grid-middle"> ' +
            '<div class="GridLex-col-6_md-12"> ' +
            '<div class="restaurant-type"> ' +
            '<div class="image"> ' +
            '<img src="./Thefoody - Order Online Portal Responsive HTML Template_files/01.jpg" alt="image"> ' +
            '</div> ' +
            '<div class="content"> ' +
            '<h4>' + mealName + '</h4> ' +
            '<p>' + priceString + '</p> ' +
            '</div> ' +
            '</div> ' +
            '</div> ' +
            '<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss"> ' +

            '</div> ' +
            '<div class="GridLex-col-2_xs-4_xss-12"> ' +
            '<div class="res-btn label label-danger"> ' +
            'Add ' +
            '</div> ' +

            '</div> ' +
            '</div> ' +
            '</a>';

        $(htmlData).appendTo('#foodblock');

    });
}